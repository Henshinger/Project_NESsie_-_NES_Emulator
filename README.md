# Overview

This is kind of a sick and sadistic way of harming myself...seriously, only the criminally insane would emulate hardware but here I am and welcome, to NESsie. 

# Programming Language
Language being used is C. I chose this language for numerious reasons. One is I have grown quite an affinity towards C. It's the language I used in grad school and overall it's a fun language. Two, it's a structural programming language. Yes OOP is all the rave, but in reality I just don't feel it at times. CPU's in a nutshell do one instruction at a time, so from that standpoint I really don't need to worry about all the intricacies of OOP. 

# System
I am designing this to run on a Quad Core processor, primary on the Raspberry Pi. The reason why 

## Emulate close to the original

The idea is to emulate as close as possible. With this, the idea is to keep everything agnostic from each other and only communicate with one another through pipelines.  

## Important Excell with notes

I will be updating this excell with things I can't put in here, such as a color coded grid for instructions implimented. 
[document](https://docs.google.com/spreadsheets/d/125cMr9msxMurdQrqtuXGiw0YXN2Ofs8r_6YCbs5Fz8g/edit?usp=sharing)

# Donations
If you would like to donate to the project, a simple donation of about 3.50 would be extremely helpful.

Test