#ifndef NES_2A03_RAM
#define NES_2A03_RAM

#include <stdio.h>
#include <stdint.h>

  //CPU RAM 2KB
  uint8_t _6502_RAM[0XFFFF];

  uint8_t _2A03_mem_ZA_fetch(const char *hexstring);
  void _2A03_mem_ZA_put(const char *hexstring, uint8_t value);

  uint8_t _2A03_mem_AX_fetch(const char *hexstring);
  void _2A03_mem_AX_put(const char *hexstring, uint8_t value);

  uint8_t _2A03_mem_AY_fetch(const char *hexstring);
  void _2A03_mem_AY_put(const char *hexstring, uint8_t value);

  uint8_t _2A03_mem_A_fetch(const char *hexstring);
  void _2A03_mem_AB_put(const char *hexstring, uint8_t value);

  //Debug Dump
  void _2A03_mem_dump(const uint16_t addrLoc);



#endif