#ifndef NES_6502_CPU
#define NES_6502_CPU

#include <stdio.h>
#include <stdint.h>

typedef uint8_t REGISTER_8Bit;
typedef uint8_t _8BIT;

  //Registers
  REGISTER_8Bit A;
  REGISTER_8Bit X;
  REGISTER_8Bit Y;
  REGISTER_8Bit S;
  REGISTER_8Bit P;
  uint16_t PC;

  //In a progr
  _8BIT CACHE;

  enum AddressingMode{
    IMMEDIATE,
    ZERO_PAGE,
    ZERO_PAGE_X,
    ZERO_PAGE_Y,
    ABSOLUTE,
    ABSOLUTE_X,
    ABSOLUTE_Y,
    INDIREXT_X,
    INDIREXT_Y

  };

  struct CPU_Instruction{
    uint8_t op_code;
    enum AddressingMode mode;
    uint8_t bytes;
    uint8_t cycles;

  };


  //CPU Power On and Reset
  void cpu_powerOn();
  void cpu_reset();
  struct CPU_Instruction decode_instruction(const char *op_code_str);
  void fetch_rom_byte();

  //Instructions
  void AND(const uint8_t memValue);
  void CMP(const uint8_t memValue);
  void LDA(const uint8_t memValue);

  //OP Codes
  void AND_29(const char *mem_addr_str);

  void CMP_C9(const char *mem_addr_str);

  void LDA_AD(const char *mem_addr_str);

  uint8_t hexstr_to_8_bit(const char *hex_str);




#endif