#ifndef NES_2A03_CPU_TESTS_H
#define NES_2A03_CPU_TESTS_H

#include <stdio.h>
#include <stdint.h>
#include "../inc/NES_2A03_CPU.h"

enum Program_Flags
{
  C_BIT,
  Z_BIT,
  I_BIT,
  D_BIT, /*Not Used*/
  B_BIT,
  A_BIT, /*Not used*/
  V_BIT,
  N_BIT
};


uint8_t run_CPU_Tests(uint8_t quit_on_error);
uint8_t get8itValueFromMemory(enum AddressingMode addrMode, const char *hex_str);
uint8_t get_A_Value();
uint8_t check_P_Flag_status(enum Program_Flags bit);

/*LDA TEST Driver and Test Case functions*/
uint8_t LDA_ZERO_TEST_CASE(const uint8_t value);
uint8_t LDA_BIT_N_Z_NOT_SET_TEST_CASE(const uint8_t value);
uint8_t LDA_BIT_7_SET_TEST_CASE(const uint8_t value);
uint16_t RUN_LDA_AD_TEST();

/*CMP Test Driver and Test Case function*/
uint8_t CMP_BIT_N_SET_TEST_CASE(const uint8_t value);
uint8_t CMP_LESS_THAN_TEST_CASE(const uint8_t value);
uint8_t CMP_EQUAL_TEST_CASE(const uint8_t value);
uint8_t CMP_GREATER_THAN_TEST_CASE(const uint8_t value);
uint16_t RUN_CMP_C9_TEST();

uint8_t AND_Z_BIT_TEST_CASE(const uint8_t value);
uint16_t RUN_AND_29_TEST();


uint8_t RUN_TEST(uint8_t (*_CPU_INSTRUCTION()), uint8_t (*_TEST_CASE(const uint8_t value)),
                 const char *_CPU_INSTRUCTION_PARM, enum AddressingMode addrMode);


#endif
