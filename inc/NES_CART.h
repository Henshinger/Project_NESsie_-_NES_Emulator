#ifndef NES_CART_H
#define NES_CART_H

#include <stdlib.h>
#include <stdint.h>

//PRG ROM
//PRG RAM
//CHR


extern uint16_t _00KB;
extern uint16_t _04KB;
extern uint16_t _08KB;
extern uint16_t _16KB;
extern uint16_t _32KB;


struct nes_board_spec{
	uint16_t rom_size;
	uint16_t ram_size;
	uint16_t chr_size;
	char    *name;
};

struct nes_cart{
	uint8_t *rom;
	uint8_t *ram;
	uint8_t *chr;
};

struct nes_cart _cart;
struct nes_board_spec _board_spec;

void NROM_Init();

uint8_t rom_fetch(uint16_t addr);
void rom_put(uint16_t addr, uint16_t value);
void rom_load(char *);
void cart_rom_dump(const uint16_t addrLoc);
void insert_rom();
void eject_rom();


#endif

