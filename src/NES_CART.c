#include "../inc/NES_CART.h"

uint16_t _00KB = 0x0000;
uint16_t _04KB = 0x1000;
uint16_t _08KB = 0x2000;
uint16_t _16KB = 0x4000;
uint16_t _32KB = 0x8000;

void NROM_Init(){

	_board_spec = (struct nes_board_spec){_16KB,
		                                  _00KB,
		                                  _08KB,
		                                  "NROM"};
	printf("Board Name:%s ROM Size: %X RAM Size: %X CHR Size: %X\n", 
		    _board_spec.name, _board_spec.rom_size, _board_spec.ram_size, 
		    _board_spec.chr_size);
//Initialize Cartridge

	_cart.rom =  (uint8_t *)malloc(0x4000 * sizeof(uint8_t));
	_cart.ram = NULL;
	_cart.chr = (uint8_t *)malloc(0x2000 * sizeof(uint8_t));

	size_t n= sizeof(_cart.rom)/sizeof(_cart.rom[0]);

  if(_cart.rom == NULL && _cart.chr == NULL) {
    printf("malloc of for ROM or CHR failed!\n");   // could also call perror here
  }
}

void insert_rom(){
	
}

void eject_rom(){
	free(_cart.rom);
	free(_cart.ram);
	free(_cart.chr);
}

uint8_t rom_fetch(uint16_t addr){
	if(addr > _board_spec.rom_size)
		return 0xFF;
	else
	    return _cart.rom[addr];
}

void rom_put(uint16_t addr, uint16_t value){
  _cart.rom[addr] = value;	
}

void rom_load(char *hex_code){

	char fullword[2];
	uint16_t rom_index = 0x00;
    

	for(int i = 0x0000; i < 0x0018; i++){
	
		if(hex_code[i] != ' '){
		memcpy( fullword, &hex_code[i], 2 );
		
		_cart.rom[rom_index] = (uint8_t)strtol(fullword, NULL, 16);

		i++;
		rom_index++;
	  }
	}

}

void cart_rom_dump(const uint16_t addrLoc){
  int len = 0; 
  uint16_t addrEnd = addrLoc + 0x0060;
  for(uint16_t addr = addrLoc; addr < addrEnd; addr++){
			
    if(len < 15){
	  if(_cart.rom[addr] < 0x0010)   
	    printf("0");

	    printf("%X ",  _cart.rom[addr]);
	    len++;
	  }
      else{
	    printf("\n");
	    len = 0;
	  }
	
  }
  printf("\n");
}


