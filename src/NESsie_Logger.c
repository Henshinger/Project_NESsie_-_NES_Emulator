#include "../inc/NESsie_Logger.h"

void print8BitRegister(char reg, uint8_t regCont){

	unsigned i;
	printf("Reg %c: Hex: %X - Bin: ", reg, regCont);
	printf("[ ");

	for(i = 1 << 7; i > 0; i = i / 2)
	{
		(regCont & i) ? printf("1 "): printf("0 ");
	}

	printf("]\n");

}

void print16BitRegister(uint16_t regCont){

	unsigned i;
	printf("Reg PC: Hex: %X - Bin: ", regCont);
	printf("[ ");

	for(i = 1 << 15; i > 0; i = i / 2)
	{
		(regCont & i) ? printf("1 "): printf("0 ");
	}

	printf("]\n");

}