#include "../inc/NES_2A03_RAM.h"
#include "../inc/NES_2A03_CPU.h"

uint8_t _2A03_mem_ZA_fetch(const char *hexstring){

	  uint8_t  value;  

    uint8_t addrLoc = (uint8_t)strtol(hexstring, NULL, 16);
    value = _6502_RAM[addrLoc];

    printf("The Value at Address : $%s is %X\n", hexstring, value);

    return value;
}

uint8_t _2A03_mem_AX_fetch(const char *hexstring){

    uint8_t  value;  

    uint16_t addrLoc = (uint16_t)strtol(hexstring, NULL, 16) + X;
    value = _6502_RAM[addrLoc];

    printf("The Value at Address : $%s is %X\n", hexstring, value);

    return value;
}

uint8_t _2A03_mem_AY_fetch(const char *hexstring){

    uint8_t  value;  

    uint16_t addrLoc = (uint16_t)strtol(hexstring, NULL, 16) + Y;
    value = _6502_RAM[addrLoc];

    printf("The Value at Address : $%s is %X\n", hexstring, value);

    return value;
}

uint8_t _2A03_mem_AB_fetch(const char *hexstring){

    uint8_t  value;  

    uint16_t addrLoc = (uint16_t)strtol(hexstring, NULL, 16);
    value = _6502_RAM[addrLoc];

    printf("The Value at Address : $%s is %X\n", hexstring, value);

    return value;
}

void _2A03_mem_ZA_put(const char *hexstring, uint8_t value){
    uint8_t addrLoc = (uint8_t)strtol(hexstring, NULL, 16);
    _6502_RAM[addrLoc] = value;

    if(_6502_RAM[addrLoc] < 0x0010)   
      printf("The Value at Address : $%X is 0%X\n", hexstring, value);
    else
      printf("The Value at Address : $%X is %X\n", hexstring, value);
}

void _2A03_mem_AX_put(const char *hexstring, uint8_t value){
    uint16_t addrLoc = (uint16_t)strtol(hexstring, NULL, 16) + X;
    _6502_RAM[addrLoc] = value;

    if(_6502_RAM[addrLoc] < 0x0010)   
      printf("The Value at Address : $%X is 0%X\n", addrLoc, value);
    else
      printf("The Value at Address : $%X is %X\n", addrLoc, value);

}

void _2A03_mem_AY_put(const char *hexstring, uint8_t value){
    uint16_t addrLoc = (uint16_t)strtol(hexstring, NULL, 16) + Y;
    _6502_RAM[addrLoc] = value;

    if(_6502_RAM[addrLoc] < 0x0010)   
      printf("The Value at Address : $%X is 0%X\n", addrLoc, value);
    else
      printf("The Value at Address : $%X is %X\n", addrLoc, value);

}

void _2A03_mem_AB_put(const char *hexstring, uint8_t value){
    uint16_t addrLoc = (uint16_t)strtol(hexstring, NULL, 16);
    _6502_RAM[addrLoc] = value;

    if(_6502_RAM[addrLoc] < 0x0010)   
      printf("The Value at Address : $%X is 0%X\n", addrLoc, value);
    else
      printf("The Value at Address : $%X is %X\n", addrLoc, value);

}

//Debug Dump
void _2A03_mem_dump(const uint16_t addrLoc){
  int len = 0; 
  for(uint16_t addr = addrLoc; addr < 0x0060; addr++){
			
    if(len < 15){
	  if(_6502_RAM[addr] < 0x0010)   
	    printf("0");

	    printf("%X ",  _6502_RAM[addr]);
	    len++;
	  }
      else{
	    printf("\n");
	    len = 0;
	  }
	
  }
  printf("\n");
}


