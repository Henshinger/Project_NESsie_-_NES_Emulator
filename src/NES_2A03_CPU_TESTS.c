#include "../inc/NES_2A03_CPU_TESTS.h"


uint8_t get8itValueFromMemory(enum AddressingMode addrMode, const char *hex_str){
  switch(addrMode){
    case IMMEDIATE   : return hexstr_to_8_bit(hex_str);
                       break;
  /*case ZERO_PAGE   :
                     break;
    case ZERO_PAGE_X :
                     break;
    case ZERO_PAGE_Y :
                     break;*/
    case ABSOLUTE    : return _2A03_mem_AB_fetch(hex_str);
                       break;
    case ABSOLUTE_X  : return _2A03_mem_AX_fetch(hex_str);
                       break;
   /* case ABSOLUTE_Y  :
                     break;
    case INDIREXT_X  :
                     break;
    case INDIREXT_Y  :
                     break;*/
    default          : return 0xFF;


  }
}

uint8_t get_A_Value(){
  return A;
}

//Status Register BIT Checkerr Helper Fucntions
uint8_t check_P_Flag_status(enum Program_Flags bit){

  if((P & (1<< bit)))
    return 0x01;
  else
    return 0x00;
}

/** Runs all the CPU Tests and Returns the return code
 *
 */
uint8_t run_CPU_Tests(uint8_t quit_on_error){

  uint16_t rc;
  uint8_t i;

   for (i = 0x00; i < 0xFF; i++ ){
    switch(i){
      case 0x29: rc = RUN_AND_29_TEST();
                 printf("OPCODE : 29 - AND TEST : ");
                 (rc == 0x0000) ? printf("PASS\n") : printf("FAIL RC:%X\n",rc);
                 break;
      case 0xAD: rc = RUN_LDA_AD_TEST();
                 printf("OPCODE : AD - LDA TEST : ");
                 (rc == 0x0000) ? printf("PASS\n") : printf("FAIL RC:%X\n",rc);
                 break;
      case 0xC9: rc = RUN_CMP_C9_TEST();
                 printf("OPCODE : C9 - CMP TEST : ");
                 (rc == 0x0000) ? printf("PASS\n") : printf("FAIL RC:%X\n",rc);
                 break;
  
    }
   }

  if(rc != 0x0000 && quit_on_error)
    return i;
  else
   return 0xFFFF;

}

/**
 *
 */
uint8_t RUN_TEST(uint8_t (*_CPU_INSTRUCTION()), uint8_t (*_TEST_CASE(const uint8_t value)),
                 const char *_CPU_INSTRUCTION_PARM, enum AddressingMode addrMode){
  
  _CPU_INSTRUCTION(_CPU_INSTRUCTION_PARM);

  uint8_t value = get8itValueFromMemory(addrMode, _CPU_INSTRUCTION_PARM);

  return _TEST_CASE(value);
}

