#include "../../inc/NES_2A03_CPU_TESTS.h"

/*****************************************************************************/
/*****************************LDA TEST CASES**********************************/
/*****************************************************************************/

/**
 *
 */
uint8_t LDA_BIT_Z_SET_TEST_CASE(const uint8_t value){

  if(get_A_Value() != value){
    return 0x01;
  }

  if(!check_P_Flag_status(Z_BIT)){
    return 0x02;
  }

  if(check_P_Flag_status(N_BIT)){
    return 0x03;
  }

  return 0x00;
}

/**
 *
 */
uint8_t LDA_BIT_N_SET_TEST_CASE(const uint8_t value){

  if(get_A_Value() != value){
    return 0x01;
  }

  if(check_P_Flag_status(Z_BIT)){
    return 0x02;
  }

  if(!check_P_Flag_status(N_BIT)){
    return 0x03;
  }

  return 0x00;
}

/**
 *
 */
uint8_t LDA_BIT_N_Z_NOT_SET_TEST_CASE(const uint8_t value){

  if(get_A_Value() != value){
    return 0x01;
  }

  if(check_P_Flag_status(Z_BIT)){
    return 0x02;
  }

  if(check_P_Flag_status(N_BIT)){
    return 0x03;
  }

  return 0x00;
}

/**
 *
 */
uint16_t RUN_LDA_AD_TEST(){

  //Zero Test Case
  uint8_t test_rc = 0x00;

  test_rc = RUN_TEST(LDA_AD, LDA_BIT_Z_SET_TEST_CASE, "0004", ABSOLUTE);

  if(test_rc != 0)
    return (0x00 <<8) | test_rc;
 

  test_rc = RUN_TEST(LDA_AD, LDA_BIT_N_SET_TEST_CASE, "0000", ABSOLUTE);

  if(test_rc != 0)
    return (0x01 <<8) | test_rc;

  test_rc = RUN_TEST(LDA_AD, LDA_BIT_N_Z_NOT_SET_TEST_CASE, "0001", ABSOLUTE);

  if(test_rc != 0)
    return (0x02 <<8) | test_rc;

  return 0x0000;

}
/*****************************************************************************/