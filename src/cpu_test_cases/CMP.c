#include "../../inc/NES_2A03_CPU_TESTS.h"

/*CMP Test Driver and Test Case function*/
uint8_t CMP_BIT_N_SET_TEST_CASE(const uint8_t value){

  if(!check_P_Flag_status(N_BIT)){
    return 0x01;
  }

  return 0x00;
}

uint8_t CMP_LESS_THAN_TEST_CASE(const uint8_t value){
  if(check_P_Flag_status(C_BIT)){
    return 0x01;
  }

  if(check_P_Flag_status(Z_BIT)){
    return 0x02;
  }

  return 0x00;
}

uint8_t CMP_EQUAL_TEST_CASE(const uint8_t value){
  if(!check_P_Flag_status(C_BIT)){
    return 0x01;
  }

  if(!check_P_Flag_status(Z_BIT)){
    return 0x02;
  }

  return 0x00;
}

uint8_t CMP_GREATER_THAN_TEST_CASE(const uint8_t value){
  if(!check_P_Flag_status(C_BIT)){
    return 0x01;
  }

  if(check_P_Flag_status(Z_BIT)){
    return 0x02;
  }

  return 0x00;
}

uint16_t RUN_CMP_C9_TEST(){
  //Equals Test Case
  uint16_t test_rc = 0x00;

  A = 0x64;

  test_rc = RUN_TEST(CMP_C9, CMP_EQUAL_TEST_CASE, "0064", IMMEDIATE);

  if(test_rc != 0x00)
    return (0x00 <<8) | test_rc;

  //Greater Than Test
  A = 0xAD;
  test_rc = RUN_TEST(CMP_C9, CMP_GREATER_THAN_TEST_CASE, "0064", IMMEDIATE);
  
  if(test_rc != 0x00)
    return (0x01 <<8) | test_rc;

  //Check negative bit
  A = 0x64;
  test_rc = RUN_TEST(CMP_C9, CMP_BIT_N_SET_TEST_CASE, "00AD", IMMEDIATE);
  
  if(test_rc != 0x00)
    return (0x01 <<8) | test_rc;

  return 0x00;
}
