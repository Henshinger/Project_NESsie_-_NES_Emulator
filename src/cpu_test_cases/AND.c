#include "../../inc/NES_2A03_CPU_TESTS.h"

/*AND Test Driver and Test Case function*/
uint8_t AND_Z_BIT_TEST_CASE(const uint8_t value){

  if(!check_P_Flag_status(Z_BIT)){
    return 0x01;
  }

  return 0x00;
}

uint8_t AND_N_BIT_TEST_CASE(const uint8_t value){

  if(!check_P_Flag_status(N_BIT)){
    return 0x01;
  }

  return 0x00;
}

uint16_t RUN_AND_29_TEST(){
  //Equals Test Case
  uint16_t test_rc = 0x00;

  A = 0x00;

  test_rc = RUN_TEST(AND_29, AND_Z_BIT_TEST_CASE, "00", IMMEDIATE);

  if(test_rc != 0x00)
    return (0x00 <<8) | test_rc;

   A = 0xFF;

  test_rc = RUN_TEST(AND_29, AND_N_BIT_TEST_CASE, "FF", IMMEDIATE);

  if(test_rc != 0x00)
    return (0x01 <<8) | test_rc;


  return 0x00;
}
