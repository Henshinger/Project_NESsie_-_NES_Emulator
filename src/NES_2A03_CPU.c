#include "../inc/NES_2A03_CPU.h"

void cpu_powerOn(){
	printf("2A03 CPU powering on...\n");

	A = 0x00;
	X = 0x00;
	Y = 0x00;
	P = 0x34;
	S = 0xFD;
	PC = 0x00;
}

void cpu_reset(){

}

struct CPU_Instruction decode_instruction(const char *op_code_str)
{
  uint8_t op_code = hexstr_to_8_bit(op_code_str);

  switch(op_code){

    case 0xAD: return (struct CPU_Instruction){0xAD, ABSOLUTE, 2, 2};

    default : printf("Invalid OP Code or OP Code not Implimented\n");
              return (struct CPU_Instruction){0xFF, IMMEDIATE, 0xFF, 0xFF};
  }
}

void AND(uint8_t memValue){

  //AND value with with A and store in A
  A &= memValue;

  //Is BIT 7(NEG) High?
  (A & (1 << 7)) ? (P |= (1 << 7)) : (P &= ~(1 << 7));

  //Is result 0?
  (A == 0x00) ? (P |= (1 << 1)) : (P &= ~(1 << 1));

}

void AND_29(const char *mem_addr_str){

  AND(hexstr_to_8_bit(mem_addr_str));

}

void CMP(uint8_t memValue){

  /*
  printf("A : %X\n", A);
  printf("memValue : %X\n", memValue);
  print8BitRegister('P', P);*/

  //Tests for results 7 bit(Negative) is high
  if(((A - memValue) & (1 << 7)))
     P |= (1 << 7);
  else
     P &= ~(1 << 7);

  //Checking if result is 0
  if(A == memValue)
      P |= (1 << 1);
  else
      P &= ~(1 << 1);

  if(A >= memValue)
    P |= (1 << 0);
  else
    P &= ~(1 << 0);
}
  
void CMP_C9(const char *hex_value_str){
  CMP(hexstr_to_8_bit(hex_value_str));
}


void LDA(uint8_t memValue){


   A = memValue;

    //Checking if Accumukator is 0
    if(A == 0x00){
      P |= (1 << 1);
    }
    else{
      P &= ~(1 << 1);
    }

    //Tests for 7 bit(Negative) is high
    if((A & (1 << 7))){
      P |= (1 << 7);

    }else{
      P &= ~(1 << 7);
    }
}

void LDA_AD(const char *mem_addr_str){

    LDA(_2A03_mem_AB_fetch(mem_addr_str));
}


uint8_t hexstr_to_8_bit(const char *hex_str){
  return (uint8_t)strtol(hex_str, NULL, 16);
}