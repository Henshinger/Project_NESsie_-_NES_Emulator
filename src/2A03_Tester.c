#include "../inc/NES_2A03_CPU.h"
#include "../inc/NES_2A03_CPU_TESTS.h"
#include "../inc/NES_2A03_RAM.h"
#include "../inc/NES_CART.h"
#include "../inc/NESsie_Logger.h"

#include <stdio.h>

void system_on();

int main(){

	system_on();

	rom_load("AD0A AD0A AD0A AD0A AD0A");

	cart_rom_dump(0x0000);

	print8BitRegister('A', A);
	print8BitRegister('X', X);
	print8BitRegister('Y', Y);
	print8BitRegister('P', P);
	print8BitRegister('S', S);
	print16BitRegister(PC);

	_2A03_mem_ZA_fetch("0000");
	_2A03_mem_ZA_put("0000", 0xFF);
	_2A03_mem_ZA_put("0001", 0x0F);
	_2A03_mem_ZA_put("0002", 0xAA);
	_2A03_mem_ZA_put("0003", 0xFF);
	_2A03_mem_ZA_put("0004", 0x00);
	_2A03_mem_ZA_put("0005", 0xBF);
	_2A03_mem_ZA_put("0006", 0xDD);
	_2A03_mem_ZA_put("0007", 0xFF);
	_2A03_mem_ZA_put("0008", 0xC4);
	_2A03_mem_ZA_put("0009", 0xC7);
	_2A03_mem_ZA_put("000A", 0xB4);
	_2A03_mem_ZA_put("000B", 0xAF);
	_2A03_mem_ZA_put("000C", 0xFA);
	_2A03_mem_ZA_put("000D", 0xFF);
	_2A03_mem_ZA_put("000E", 0xFF);
	_2A03_mem_ZA_put("000F", 0xFF);


	_2A03_mem_dump(0x0000);

	struct CPU_Instruction cur_instruction = decode_instruction("AD");

	printf("%X %d %d %d \n", cur_instruction.op_code, cur_instruction.mode, 
		                     cur_instruction.bytes ,cur_instruction.cycles);


	/*
	STA_8D("0005");*/

	_2A03_mem_ZA_fetch("0007");

	X = 0x0A;

	_2A03_mem_AX_put("0007", 0xDD);

	_2A03_mem_dump(0x0000);


	
 
 uint16_t test_rc = run_CPU_Tests(0x01);

 AND(hexstr_to_8_bit("FF"));


 eject_rom();

	return 0;

}

void system_on(){
	cpu_powerOn();

	NROM_Init();
	

     rom_put(0x0000,0x44);

	printf("%X\n", rom_fetch(0xFFFF));
	printf("%d\n", sizeof(_cart.ram));
	printf("%d\n", sizeof(_cart.chr));
}


