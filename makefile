NES_OBJECT_DIR=./obj
NES_SRC_DIR=./src
NES_TEST_DIR=./src/cpu_test_cases


build-all: 
	gcc -c $(NES_SRC_DIR)/NES_2A03_CPU.c -o $(NES_OBJECT_DIR)/NES_2A03_CPU.o
	gcc -c $(NES_SRC_DIR)/NES_2A03_CPU_TESTS.c -o $(NES_OBJECT_DIR)/NES_2A03_CPU_TESTS.o
	gcc -c $(NES_SRC_DIR)/NES_2A03_RAM.c -o $(NES_OBJECT_DIR)/NES_2A03_RAM.o
	gcc -c $(NES_SRC_DIR)/NES_CART.c -o $(NES_OBJECT_DIR)/NES_CART.o
	gcc -c $(NES_SRC_DIR)/NESsie_Logger.c -o $(NES_OBJECT_DIR)/NESsie_Logger.o
	gcc -c $(NES_SRC_DIR)/2A03_Tester.c -o $(NES_OBJECT_DIR)/2A03_Tester.o 
	gcc $(NES_OBJECT_DIR)/NES_2A03_RAM.o \
	    $(NES_OBJECT_DIR)/NES_2A03_CPU.o \
	    $(NES_OBJECT_DIR)/NES_2A03_CPU_TESTS.o \
	    $(NES_OBJECT_DIR)/LDA_TEST.o \
	    $(NES_OBJECT_DIR)/CMP_TEST.o \
	    $(NES_OBJECT_DIR)/NES_CART.o \
	    $(NES_OBJECT_DIR)/NESsie_Logger.o \
	    $(NES_OBJECT_DIR)/2A03_Tester.o  \
	    -o 2A03_Tester

test:
	gcc -c $(NES_SRC_DIR)/NES_2A03_CPU.c -o $(NES_OBJECT_DIR)/NES_2A03_CPU.o
	gcc -c $(NES_SRC_DIR)/NES_2A03_CPU_TESTS.c -o $(NES_OBJECT_DIR)/NES_2A03_CPU_TESTS.o
	gcc -c $(NES_TEST_DIR)/LDA.c -o $(NES_OBJECT_DIR)/LDA_TEST.o
	gcc -c $(NES_TEST_DIR)/CMP.c -o $(NES_OBJECT_DIR)/CMP_TEST.o
	gcc -c $(NES_TEST_DIR)/AND.c -o $(NES_OBJECT_DIR)/AND_TEST.o
	gcc -c $(NES_SRC_DIR)/NES_2A03_RAM.c -o $(NES_OBJECT_DIR)/NES_2A03_RAM.o
	gcc -c $(NES_SRC_DIR)/NES_CART.c -o $(NES_OBJECT_DIR)/NES_CART.o
	gcc -c $(NES_SRC_DIR)/NESsie_Logger.c -o $(NES_OBJECT_DIR)/NESsie_Logger.o
	gcc -c $(NES_SRC_DIR)/2A03_Tester.c -o $(NES_OBJECT_DIR)/2A03_Tester.o 
	gcc $(NES_OBJECT_DIR)/NES_2A03_RAM.o \
	    $(NES_OBJECT_DIR)/NES_2A03_CPU.o \
	    $(NES_OBJECT_DIR)/NES_2A03_CPU_TESTS.o \
	    $(NES_OBJECT_DIR)/LDA_TEST.o \
	    $(NES_OBJECT_DIR)/CMP_TEST.o \
	    $(NES_OBJECT_DIR)/AND_TEST.o \
	    $(NES_OBJECT_DIR)/NES_CART.o \
	    $(NES_OBJECT_DIR)/NESsie_Logger.o \
	    $(NES_OBJECT_DIR)/2A03_Tester.o  \
	    -o 2A03_Tester

comp-all: 
	gcc -c $(NES_SRC_DIR)/NES_2A03_CPU.c -o $(NES_OBJECT_DIR)/NES_2A03_CPU.o
	gcc -c $(NES_SRC_DIR)/NES_2A03_RAM.c -o $(NES_OBJECT_DIR)/NES_2A03_RAM.o
	gcc -c $(NES_SRC_DIR)/NES_CART.c -o $(NES_OBJECT_DIR)/NES_CART.o
	gcc -c $(NES_SRC_DIR)/NESsie_Logger.c -o$(NES_OBJECT_DIR)/NESsie_Logger.o
	gcc -c $(NES_SRC_DIR)/2A03_Tester.c -o $(NES_OBJECT_DIR)/2A03_Tester.o

clean:
	rm $(NES_OBJECT_DIR)/* \
	   ./2A03_Tester